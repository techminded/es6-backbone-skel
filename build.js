  var builder = require('systemjs-builder'),
    path = require('path');

  builder.build('main', {
    baseURL: path.resolve('js/app'),

    //map: path.resolve('shared_map'),
    // any map config
    map: {
        lib: 'lib',

        jquery: 'lib/jquery-bundle',
        'lib/backbone': {
            'underscore': 'lib/underscore'
        }
    }

   // etc. any SystemJS config

  }, 'build/app-bundle.js').then(function() {

    console.log('Build complete');
  }).catch(function(err) {

    console.log('Build error');
    console.log(err);
  });

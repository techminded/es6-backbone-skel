module.exports = function(grunt) {

    grunt.initConfig({
        exec: {
            buildmain: 'node build.js'

        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['js/app/lib/jquery/jquery.js', 'js/app/lib/jquery/jquery-jelect.js'],
                dest: 'js/app/lib/jquery-bundle.js'
            }
        },
        uglify: {
            build: {
                files: {
                    'build/app-bundle.min.js': 'build/app-bundle.js'
                }
            }

        }
    });
    grunt.registerTask("mkbuild", function() {
        grunt.file.mkdir('build');
    });

    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask("build", ["mkbuild", "concat", "exec:buildmain", "uglify:build"]);

    grunt.registerTask('default', ['build']);

};

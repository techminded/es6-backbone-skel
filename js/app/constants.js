"format register";

System.register("constants", [], function($__export) {
    "use strict";
    var __moduleName = "constants";
    var constants;
    return {
        setters: [],
        execute: function() {
            constants = $__export('constants', (function() {
                return {foo: 'bar'};
            })());
        }
    };
});

import $ from "../lib/jquery-bundle";
import _ from "../lib/underscore";
import Backbone from "../lib/backbone";


var { Model, View, Collection, Router, LocalStorage } = Backbone;

export class TestView2 extends View {

    constructor(options) {
        this.options = options;

        super(options);
    }

    render() {
        $('.jelect').jelect();

        return this;
    }
}
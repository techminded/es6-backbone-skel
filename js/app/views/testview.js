import $ from "../lib/jquery-bundle";
import _ from "../lib/underscore";
import Backbone from "../lib/backbone";


var { Model, View, Collection, Router, LocalStorage } = Backbone;

export class TestView extends View {

    constructor(options) {
        this.options = options;
        this.template = _.template($(options.tpl.hello).html());
        super(options);
    }

    render() {
        this.$el.html(this.template({
            helloMessage: this.options.l11n.helloMessage
        }));
        return this;
    }
}